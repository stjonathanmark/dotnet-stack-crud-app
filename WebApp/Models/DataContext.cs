﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace WebApp.Models
{
    public class DataContext : DbContext
    {
        public DbSet<Person> Persons { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var personsTbl = modelBuilder.Entity<Person>();

            personsTbl.ToTable("Persons", "dbo");
            personsTbl.HasKey(p => p.Id);
            personsTbl.Property(p => p.FirstName).IsRequired();
            personsTbl.Property(p => p.LastName).IsRequired();
        }
    }
}