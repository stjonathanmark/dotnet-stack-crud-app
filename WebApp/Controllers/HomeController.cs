﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            var db = new DataContext();

            var people = db.Persons.ToList();

            return View(people);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(string firstName, string lastName)
        {
            var person = new Person();

            person.FirstName = firstName;
            person.LastName = lastName;

            var db = new DataContext();

            db.Persons.Add(person);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Update(int id)
        {
            var db = new DataContext();

            var person = db.Persons.FirstOrDefault(p => p.Id == id);

            return View(person);
        }

        [HttpPost]
        public ActionResult Update(Person person)
        {
            var db = new DataContext();

            db.Persons.Attach(person);
            db.Entry(person).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            var db = new DataContext();

            var person = db.Persons.FirstOrDefault(p => p.Id == id);

            db.Persons.Remove(person);
            db.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}